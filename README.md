# Installing a Virtual Private Development Server

This document will guide you through the installation of a Virtual Private Server (VPS) for development purposes.

The VPS installed going through this guide serves as a reference platform for several courses on the PBA for Web Development at Copenhagen School of Design and Technology (KEA, Københavns Erhvervsakademi).

We use Linode as an infrastructure-as-a-service provider for this guide, and while Linode services are not free, they are very affordable.
Most of this guide would work very similar with other service providers, but we use Linode as a reference platform and will not consider other platforms.

We suggest you fork this repository into a private repository on GitLab and make notes about the installation as you go through it.
You are more than welcome to suggest improvements to the guide via a merge request.

There are a few installation procedures that differ between Linux, macOS, and Windows.
While we do our best to make this work on all platforms, things sometimes change, so a few things might not look exactly like they do in this guide.
Use your common sense and problem-solving skills to work your way through such issues.


## Creating SSH Keys

The first thing we need to do is to create an SSH public/private key-pair.
When using SSH with public/private keys, we get a very high level of security, and we do not have to use passwords if we do not want to.

**MacOS and Linux:**
If your computer is running macOS or Linux, check if you already have SSH keys on your computer by running this command in a terminal:

```bash
$ ls ~/.ssh
```

If you get a list of files `id_rsa` and `id_rsa.pub` you already have keys on your system.
These are your **private** and **public** keys, respectively.
Your keys might have a slightly different name, such as `id_ed25519` and `id_es25519.pub`, which just means that your keys use a different encryption algorithm, which is not a problem.

If you do not have any keys listed, create a new public/private key-pair using this command:

```bash
$ ssh-keygen -t ed25519 -a 100
```

You must answer a few questions, but you can leave everything blank or default by simply pressing RETURN on every question.

> You can leave the password blank by simply pressing RETURN when asked for password and password confirmation. Doing so means you don't have to enter any passwords when working with SSH. It is safe to do so as long as you keep your private key safe and private.

**Windows:**
If your computer is running Windows, please follow this guide to create a public/private key-pair.
We recommend that you use the first method described (OpenSSH).

[https://phoenixnap.com/kb/generate-ssh-key-windows-10/](https://phoenixnap.com/kb/generate-ssh-key-windows-10/)


## Create A Linode Account

You need a Linode account to set up a VPS with Linode.
On the link below, you will reveive a $100, 60-day credit when finishing the registration:

[https://www.linode.com/?r=1cc5604a5a6964df89a68075a4919e770101e7e1](https://www.linode.com/?r=1cc5604a5a6964df89a68075a4919e770101e7e1)

**DISCLAIMER:** The link above is my referral link - I will also receive some credit from you signing up via this link.
You are welcome to sign up some other way if you prefer.

We now need to upload your **public** key to Linode:

- Once logged in to your account, click your user name in the top right corner, and select *SSH Keys* under *My Profile*.
- Click the *Add an SSH Key* button.
- Supply a label (e.g., the name of your user account and machine: *henrik@minime*).
- Open the public file called e.g. ed25519.pub
- Paste the **contents** of your **public** key in the *SSH Public Key* field.

> The **contents** of your public key should look something like `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK0wmN/Cr3JXqmLW7u+g9pTh+wyqDHpSQEIQczXkVx9q gleb@reys.net`

We would also recommend that you enable two-factor authentication under *Login & Authentication*, but that is up to you.


## Create A Linode Virtual Private Server

In the left column menu, choose *Linodes*.

Click *Create Linode*, and set the following options:

- Choose a Distribution:
  - Images: Alpine 3.15
- Region: Europe, Frankfurt, DE
- Linode Plan: Under *Shared CPU* select Nanode 1GB, $5 monthly, 1GB RAM, 1 CPU, 25GB storage
- Linode Label: kea-development-server
- Root Password: *set a good, long password - keep it safe!*
- SSH Keys: *check the box next to your user name to include your SSH keys*
- Optional Add-ons - Backup: *check this if you want Linode to back up your VPS - this is recommended*

Your plan should come to $5 or $7 a month, depending on whether you selected backups or not.

Click *Create Linode* to create your new VPS.

Your VPS will now be created and started, which usually takes a few minutes.

Make a note of the *IP Address* in the top right corner: the first one is the IPv4 address, e.g., `139.162.141.12` in my case; you will need this address to reach your VPS (your IP address will be different).


## Add Your VPS To Your SSH Configuration

To make it easy to access the `kea-dev` server, we will add a configuration for it to the SSH configuration file.

If you have `vim` installed on your local computer, you can use the command:

```bash
$ vim ~/.ssh/config
```

Make sure the file looks like this, replacing the IP address with your VPS IP address:

```bash
Host kea-dev
    HostName 139.162.141.12
```

Save the file and exit.

> If you are on a Windows computer, `vim` is probably not installed on your computer.
> You can instead use a program like `Notepad`.\
> Please notice, that `Notepad` will likely add the `.txt` extension to your file.
> You will have to remove it using the command: `ren config.txt config` inside your `.ssh` directory.


## Connect to your Virtual Private Server

Now we can connect to the VPS using SSH - open a terminal or command window and type this command, and answer **'yes'** to continue:

```bash or zsh
$ ssh root@kea-dev
The authenticity of host kea-dev (139.162.141.12) can\'t be established.
ECDSA key fingerprint is SHA256:l4X3SRo3eHLVzDIbT4HF1lCQaMhzcyFI5Cjkxl9etMQ.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'kea-dev,139.162.141.12' (ECDSA) to the list of known hosts.
Welcome to Alpine!

The Alpine Wiki contains a large amount of how-to guides and general
information about administrating Alpine systems.
See <http://wiki.alpinelinux.org/>.

You can setup the system with the command: setup-alpine

You may change this message by editing /etc/motd.

localhost:~#
```

> All the commands you enter when logged in to your VPS will run on the VPS - not your local computer.


## Run Setup Alpine

Alpine Linux comes with a script to prepare the VPS by configuring a long list of settings.
Run the script `setup-alpine` as shown below, and choose appropriate values for keyboard layout, etc.
As for the later, more technical settings, you can just follow the example below.

```text
localhost:~#  setup-alpine
Available keyboard layouts:
af     at     be     by     cn     dz     fi     ge     hu     in     it     kr     lk     md     mm     nl     pl     ru     sy     tr     uz
al     az     bg     ca     cz     ee     fo     gh     id     iq     jp     kz     lt     me     mt     no     pt     se     th     tw     vn
am     ba     br     ch     de     epo    fr     gr     ie     ir     ke     la     lv     mk     my     ph     ro     si     tj     ua
ara    bd     brai   cm     dk     es     gb     hr     il     is     kg     latam  ma     ml     ng     pk     rs     sk     tm     us
Select keyboard layout [none]: us
Available variants: us-alt-intl us-altgr-intl us-chr us-colemak us-dvorak-alt-intl us-dvorak-classic us-dvorak-intl us-dvorak-l us-dvorak-r us-dvorak us-dvp us-euro us-hbs us-intl us-mac us-norman us-olpc2 us-rus us-workman-intl us-workman us
Select variant []: us-mac
 * Caching service dependencies ...                                                                                                                              [ ok ]
 * Setting keymap ...                                                                                                                                            [ ok ]
Enter system hostname (short form, e.g. 'foo') [localhost]: kea-dev
Available interfaces are: eth0.
Enter '?' for help on bridges, bonding and vlans.
Which one do you want to initialize? (or '?' or 'done') [eth0]
Ip address for eth0? (or 'dhcp', 'none', '?') [139.162.141.12]
Netmask? [255.255.0.0]
Gateway? (or 'none') [139.162.141.1]
Configuration for eth0:
  type=static
  address=139.162.141.12
  netmask=255.255.0.0
  gateway=139.162.141.1
Do you want to do any manual network configuration? [no]
DNS domain name? (e.g 'bar.com') []
DNS nameserver(s)? [139.162.138.5 139.162.139.5 139.162.130.5 139.162.131.5 139.162.132.5 139.162.133.5 139.162.134.5 139.162.135.5 139.162.136.5 139.162.137.5 ]
Changing password for root
New password:
Retype password:
passwd: password for root changed by root
Which timezone are you in? ('?' for list) [UTC] ?
Africa/      Atlantic/    Canada/      EST5EDT      Factory      GMT-0        Iceland      Japan        MST7MDT      PRC          ROC          US/          Zulu
America/     Australia/   Chile/       Egypt        GB           GMT0         Indian/      Kwajalein    Mexico/      PST8PDT      ROK          UTC          posixrules
Antarctica/  Brazil/      Cuba         Eire         GB-Eire      Greenwich    Iran         Libya        NZ           Pacific/     Singapore    Universal    right/
Arctic/      CET          EET          Etc/         GMT          HST          Israel       MET          NZ-CHAT      Poland       Turkey       W-SU
Asia/        CST6CDT      EST          Europe/      GMT+0        Hongkong     Jamaica      MST          Navajo       Portugal     UCT          WET
Which timezone are you in? ('?' for list) [?] Europe
What sub-timezone of 'Europe' are you in? ('?' for list) ?
ls: Europe/: No such file or directory
Amsterdam    Belgrade     Budapest     Gibraltar    Jersey       Ljubljana    Mariehamn    Oslo         Rome         Simferopol   Tirane       Vatican      Zagreb
Andorra      Berlin       Busingen     Guernsey     Kaliningrad  London       Minsk        Paris        Samara       Skopje       Tiraspol     Vienna       Zaporozhye
Astrakhan    Bratislava   Chisinau     Helsinki     Kiev         Luxembourg   Monaco       Podgorica    San_Marino   Sofia        Ulyanovsk    Vilnius      Zurich
Athens       Brussels     Copenhagen   Isle_of_Man  Kirov        Madrid       Moscow       Prague       Sarajevo     Stockholm    Uzhgorod     Volgograd
Belfast      Bucharest    Dublin       Istanbul     Lisbon       Malta        Nicosia      Riga         Saratov      Tallinn      Vaduz        Warsaw
What sub-timezone of 'Europe' are you in? ('?' for list) Copenhagen
 * Stopping sshd ...                                                                                                                                             [ ok ]
 * Stopping RNG Daemon ...                                                                                                                                       [ ok ]
 * Stopping busybox crond ...                                                                                                                                    [ ok ]
 * Stopping chronyd ...                                                                                                                                          [ ok ]
 * Deactivating swap devices ...                                                                                                                                 [ ok ]
 * Activating swap devices ...                                                                                                                                   [ ok ]
 * Starting busybox acpid ...                                                                                                                                    [ ok ]
 * Starting chronyd ...                                                                                                                                          [ ok ]
 * Starting busybox crond ...                                                                                                                                    [ ok ]
 * Starting RNG Daemon ...                                                                                                                                       [ ok ]
 * Starting sshd ...
HTTP/FTP proxy URL? (e.g. 'http://proxy:8080', or 'none') [none]
Which NTP client to run? ('busybox', 'openntpd', 'chrony' or 'none') [chrony]
 * rc-update: chronyd already installed in runlevel `default'; skipping
 * WARNING: chronyd has already been started

Available mirrors:
1) dl-cdn.alpinelinux.org
2) uk.alpinelinux.org
3) dl-2.alpinelinux.org
...
51) mirror.ette.biz
52) mirror.lagoon.nc
53) alpinelinux.c3sl.ufpr.br

r) Add random from the above list
f) Detect and add fastest mirror from above list
e) Edit /etc/apk/repositories with text editor

Enter mirror number (1-53) or URL to add (or r/f/e/done) [1]:

Added mirror mirror.leaseweb.com
Updating repository indexes... done.
Which SSH server? ('openssh', 'dropbear' or 'none') [openssh]
 * rc-update: sshd already installed in runlevel `default'; skipping
 * WARNING: sshd has already been started
Available disks are:
  sdb	(0.5 GB QEMU     QEMU HARDDISK   )
Which disk(s) would you like to use? (or '?' for help or 'none') [none] ?

The disk you select can be used for a traditional disk install or for a
data-only install.

The disk will be erased.

Enter 'none' if you want to run diskless.

Available disks are:
  sdb	(0.5 GB QEMU     QEMU HARDDISK   )
Which disk(s) would you like to use? (or '?' for help or 'none') [none] sdb
The following disk is selected:
  sdb	(0.5 GB QEMU     QEMU HARDDISK   )
How would you like to use it? ('sys', 'data', 'lvm' or '?' for help) [?] ?

You can select between 'sys', 'data', 'lvm', 'lvmsys' or 'lvmdata'.

sys:
  This mode is a traditional disk install. The following partitions will be
  created on the disk: /boot, / (filesystem root) and swap.

  This mode may be used for development boxes, desktops, virtual servers, etc.

data:
  This mode uses your disk(s) for data storage, not for the operating system.
  The system itself will run from tmpfs (RAM).

  Use this mode if you only want to use the disk(s) for a mailspool, databases,
  logs, etc.

lvm:
  Enable logical volume manager and ask again for 'sys' or 'data'.

lvmsys:
  Same as 'sys' but use logical volume manager for partitioning.

lvmdata:
  Same as 'data' but use logical volume manager for partitioning.

The following disk is selected:
  sdb	(0.5 GB QEMU     QEMU HARDDISK   )
How would you like to use it? ('sys', 'data', 'lvm' or '?' for help) [?] sys
WARNING: The following disk(s) will be erased:
  sdb	(0.5 GB QEMU     QEMU HARDDISK   )
WARNING: Erase the above disk(s) and continue? [y/N]: y
Creating file systems...
Installing system on /dev/sdb3:
/mnt/boot is device /dev/sdb1
100% ██████████████████████████████████████████████████████████████████████████████████████████████████████==> initramfs: creating /boot/initramfs-virt
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-virt
Found initrd image: /boot/initramfs-virt
done
/boot is device /dev/sdb1

Installation is complete. Please reboot.
```

Now reboot the VPS:

```bash
kea-dev:~# reboot
```

It is not until this point in time that the Alpine Linux operating system is actually installed, so it will take about a minute before the VPS is ready for you to reconnect.


## Add User

It's not considered good practice to work with the root account, so let's make a personal account for you.

> You can name your account anything you like, but things will work best if you name your account the same as the account on your normal computer, then you will not have to specify the name of the account when using SSH.

Create your account as shown below, substituting `henrik` with your account name:

```bash
kea-dev:~# adduser henrik
```

Your account must still be able to administer the VPS - in Linux we do this via `sudo`.
We must now give your account permissions to use `sudo`, so use the command below to open the sudo config file.

```bash
kea-dev:~# visudo
```

Now add this line to the file - there's a similar line for the `root` account, and it's a good idea to add the line just below the line with root:

```text
henrik ALL=(ALL) NOPASSWD:ALL
```

> To find the line for root type: `/root` and press RETURN, then type `n` once to find the second occurance. To insert the line under root press `o` and type the line above. To save the file press `ESC` and type `:wq` and press RETURN to write and quit.

> Some Windows computers are not sending all the characters correctly to remote `vim`. If you can't save and exit `vim` using `ESC` followed by `:wq` and `ENTER`, try instead to use the sequence `ESC` `SHIFT` + `ZZ`.

Now log out as root:

```bash
kea-dev:~# exit
```

## Log In With New User

To log in with your new user:

```bash
$ ssh kea-dev
```

> If you **did not** name the account on `kea-dev` the same as the account you are using on your local computer, you will have to tell `kea-dev` the user name when you log in, so the command above becomes `ssh my_user_on_kea_dev@kea-dev`.

> Note that you will need to provide your password this time - we will fix that now.

Now copy the SSH keys from the root account to your new account, and set your account as owner:

```bash
kea-dev:~$ sudo cp -r /root/.ssh /home/$(whoami)/.
kea-dev:~$ sudo chown -R $(whoami):$(whoami) .ssh
```

Now log out again:

```bash
kea-dev:~$ exit
```

And log in with your personal account:

```bash
$ ssh kea-dev
```

This time you will not be asked for a password, because you are using the SSH key authentication.

## Clone Installation Repository

Log back into your VPS and clone this repository to the user's home folder:

```bash
kea-dev:~$ sudo apk add git

kea-dev:~$ git clone https://gitlab.com/henrikstroem/vps-reference-config.git
```

> The repository contains a number of configuration files that will help you set up your VPS.

## Configure SSHD

Copy the configuration file for the SSHD (Secure Shell) daemon from the repository to the configuration folder, and then restart the service:

```bash
kea-dev:~$ sudo cp vps-reference-config/install/sshd_config /etc/ssh/sshd_config

kea-dev:~$ sudo service sshd restart
```

> The SSH service is now configured to only allow login of normal users, and by using a public/private key pair.


## Installing Software

Configure the VPS to use extra repositories:

```bash
kea-dev:~$ sudo vim /etc/apk/repositories
```

The file should end up looking like this:

```text
http://dl-cdn.alpinelinux.org/alpine/v3.15/main
http://dl-cdn.alpinelinux.org/alpine/v3.15/community
http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing
```

You can use the arrow keys to move the cursor around the screen.
To delete the character under the cursor press `x`, to delete a whole line press `dd`.

Save the file and quit by typing `:wq` and press RETURN.

Run these commands to update the software repository lists (must be done as actual `root`, thus the use of `su root`):

```bash
kea-dev:~$ su root

kea-dev:/home/henrik# apk update && apk upgrade

fetch http://dl-cdn.alpinelinux.org/alpine/v3.15/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.15/community/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/edge/main/x86_64/APKINDEX.tar.gz
...
Executing busybox-1.32.0-r7.trigger
Executing ca-certificates-20191127-r5.trigger
Executing kmod-27-r0.trigger
Executing mkinitfs-3.4.5-r3.trigger
==> initramfs: creating /boot/initramfs-virt
Executing grub-2.04-r2.trigger
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-virt
Found initrd image: /boot/initramfs-virt
done
Executing syslinux-6.04_pre1-r6.trigger
/boot is device /dev/sda
extlinux: no previous syslinux boot sector found
OK: 217 MiB in 121 packages

kea-dev:/home/henrik# exit

kea-dev:~$
```

Now install the software we need for the VPS:

```bash
kea-dev:~$ sudo xargs apk add < vps-reference-config/install/apk-packages.txt
```


## Set Up Fail2ban

Fail2ban is a service that will monitor your log files and block out IP addresses that try to brute force login to your system.
We only listen on port 22 for SSH, so we will make use fail2ban to protect SSH traffic.

Fist, copy the fail2ban configuration files from the repository:

```bash
kea-dev:~$ sudo cp vps-reference-config/install/alpine-ssh.conf /etc/fail2ban/jail.d/.
kea-dev:~$ sudo cp vps-reference-config/install/alpine-sshd-key.conf /etc/fail2ban/filter.d/.
```

Have the service start on boot, and start it up:

```bash
kea-dev:~$ sudo rc-update add fail2ban
kea-dev:~$ sudo service fail2ban start
```


It's now time to create a public/private key-pair for your user account on the VPS, just like we did on your own computer.
This will allow you to use git and other tools from the VPS via SSH.

Go to the `.ssh` folder for your user:

```bash
kea-dev:~$ cd ~/.ssh/
```

> This is on the `kea-dev` computer, not your local computer.

If you now run the `ls` command you should see a file `authorized_keys` - that file contains a copy of your public key from your local machine and allows you to log in with this user.

Create a new public/private key pair for this computer like you did at the beginning of this guide:

```bash
$ ssh-keygen -t ed25519 -a 100
```

You can add your new public key, and the public key from your local computer, to GitLab, Github, and other sites to access via SSH.
Make sure never to share your private key.


## Configure Docker

For some courses you will need to use Docker.
Docker is already installed on the VPS, but it needs to be configured.

Run the following commands to configure Docker:

```bash
kea-dev:~$ sudo addgroup $(whoami) docker

kea-dev:~$ sudo rc-update add docker
```

Now reboot the VPS:

```bash
kea-dev:~$ sudo reboot
```

After a minute or so you can reconnect to your server, and it is now ready.


## Testing

### Testing Docker

Let's run a quick test of Docker:

```bash
kea-dev:~$ docker run hello-world

Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete
Digest: sha256:1a523af650137b8accdaed439c17d684df61ee4d74feac151b5b337bd29e7eec
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

**If you see the text above your VPS is ready to go.**

### Test Compilation During Install

Some software need to be compiled during installation.
A lot of tools are needed for this to work, but they are all installed on your VPS.

To confirm everything is working, let's install some packages in a virtual Python environment that is known to be tricky:

```bash
kea-dev:~$ python3 -V

Python 3.8.6

kea-dev:~$ python3 -m venv venv
```
Remember to type the dot in the following command - the `.` command is used to *source* the environment you just created.

```bash
kea-dev:~$ . venv/bin/activate

(venv) kea-dev:~$ pip install --upgrade pip

Collecting pip
  Downloading pip-20.3.1-py2.py3-none-any.whl (1.5 MB)
     |████████████████████████████████| 1.5 MB 8.3 MB/s
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 20.2.1
    Uninstalling pip-20.2.1:
      Successfully uninstalled pip-20.2.1
Successfully installed pip-20.3.1

(venv) kea-dev:~$ pip install django djangorestframework requests twisted

Collecting django
  Downloading Django-3.1.4-py3-none-any.whl (7.8 MB)
     |████████████████████████████████| 7.8 MB 6.4 MB/s
Collecting djangorestframework
  Downloading djangorestframework-3.12.2-py3-none-any.whl (957 kB)
     |████████████████████████████████| 957 kB 24.7 MB/s
Collecting requests
  Downloading requests-2.25.0-py2.py3-none-any.whl (61 kB)
     |████████████████████████████████| 61 kB 13.5 MB/s
Collecting twisted
  Downloading Twisted-20.3.0.tar.bz2 (3.1 MB)
     |████████████████████████████████| 3.1 MB 15.5 MB/s
Collecting asgiref<4,>=3.2.10
  Downloading asgiref-3.3.1-py3-none-any.whl (19 kB)
Collecting attrs>=19.2.0
  Downloading attrs-20.3.0-py2.py3-none-any.whl (49 kB)
     |████████████████████████████████| 49 kB 9.8 MB/s
Collecting Automat>=0.3.0
  Downloading Automat-20.2.0-py2.py3-none-any.whl (31 kB)
Collecting certifi>=2017.4.17
  Downloading certifi-2020.12.5-py2.py3-none-any.whl (147 kB)
     |████████████████████████████████| 147 kB 21.5 MB/s
Collecting chardet<4,>=3.0.2
  Downloading chardet-3.0.4-py2.py3-none-any.whl (133 kB)
     |████████████████████████████████| 133 kB 20.2 MB/s
Collecting constantly>=15.1
  Downloading constantly-15.1.0-py2.py3-none-any.whl (7.9 kB)
Collecting hyperlink>=17.1.1
  Downloading hyperlink-20.0.1-py2.py3-none-any.whl (48 kB)
     |████████████████████████████████| 48 kB 11.1 MB/s
Collecting idna<3,>=2.5
  Downloading idna-2.10-py2.py3-none-any.whl (58 kB)
     |████████████████████████████████| 58 kB 12.8 MB/s
Collecting incremental>=16.10.1
  Using cached incremental-17.5.0-py2.py3-none-any.whl (16 kB)
Collecting PyHamcrest!=1.10.0,>=1.9.0
  Downloading PyHamcrest-2.0.2-py3-none-any.whl (52 kB)
     |████████████████████████████████| 52 kB 628 kB/s
Collecting pytz
  Downloading pytz-2020.4-py2.py3-none-any.whl (509 kB)
     |████████████████████████████████| 509 kB 25.2 MB/s
Collecting six
  Downloading six-1.15.0-py2.py3-none-any.whl (10 kB)
Collecting sqlparse>=0.2.2
  Downloading sqlparse-0.4.1-py3-none-any.whl (42 kB)
     |████████████████████████████████| 42 kB 1.2 MB/s
Collecting urllib3<1.27,>=1.21.1
  Downloading urllib3-1.26.2-py2.py3-none-any.whl (136 kB)
     |████████████████████████████████| 136 kB 4.4 MB/s
Collecting zope.interface>=4.4.2
  Downloading zope.interface-5.2.0.tar.gz (227 kB)
     |████████████████████████████████| 227 kB 1.8 MB/s
Requirement already satisfied: setuptools in ./venv/lib/python3.8/site-packages (from zope.interface>=4.4.2->twisted) (49.2.1)
Using legacy 'setup.py install' for twisted, since package 'wheel' is not installed.
Using legacy 'setup.py install' for zope.interface, since package 'wheel' is not installed.
Installing collected packages: sqlparse, six, pytz, idna, attrs, asgiref, zope.interface, urllib3, PyHamcrest, incremental, hyperlink, django, constantly, chardet, certifi, Automat, twisted, requests, djangorestframework
    Running setup.py install for zope.interface ... done
    Running setup.py install for twisted ... done
Successfully installed Automat-20.2.0 PyHamcrest-2.0.2 asgiref-3.3.1 attrs-20.3.0 certifi-2020.12.5 chardet-3.0.4 constantly-15.1.0 django-3.1.4 djangorestframework-3.12.2 hyperlink-20.0.1 idna-2.10 incremental-17.5.0 pytz-2020.4 requests-2.25.0 six-1.15.0 sqlparse-0.4.1 twisted-20.3.0 urllib3-1.26.2 zope.interface-5.2.0

(venv) kea-dev:~$ pip install channels

Collecting channels
  Downloading channels-3.0.2-py3-none-any.whl (38 kB)
Requirement already satisfied: Django>=2.2 in ./venv/lib/python3.8/site-packages (from channels) (3.1.4)
Requirement already satisfied: asgiref<4,>=3.2.10 in ./venv/lib/python3.8/site-packages (from channels) (3.3.1)
Collecting daphne<4,>=3.0
  Downloading daphne-3.0.1-py3-none-any.whl (26 kB)
Requirement already satisfied: twisted[tls]>=18.7 in ./venv/lib/python3.8/site-packages (from daphne<4,>=3.0->channels) (20.3.0)
Requirement already satisfied: asgiref<4,>=3.2.10 in ./venv/lib/python3.8/site-packages (from channels) (3.3.1)
Collecting autobahn>=0.18
  Downloading autobahn-20.7.1-py2.py3-none-any.whl (1.5 MB)
     |████████████████████████████████| 1.5 MB 19.8 MB/s
Collecting cryptography>=2.7
  Downloading cryptography-3.3.1.tar.gz (539 kB)
     |████████████████████████████████| 539 kB 25.9 MB/s
  Installing build dependencies ... done
  Getting requirements to build wheel ... done
    Preparing wheel metadata ... done
Requirement already satisfied: six>=1.4.1 in ./venv/lib/python3.8/site-packages (from cryptography>=2.7->autobahn>=0.18->daphne<4,>=3.0->channels) (1.15.0)
Collecting cffi>=1.12
  Using cached cffi-1.14.4.tar.gz (471 kB)
Requirement already satisfied: pytz in ./venv/lib/python3.8/site-packages (from Django>=2.2->channels) (2020.4)
Requirement already satisfied: sqlparse>=0.2.2 in ./venv/lib/python3.8/site-packages (from Django>=2.2->channels) (0.4.1)
Requirement already satisfied: asgiref<4,>=3.2.10 in ./venv/lib/python3.8/site-packages (from channels) (3.3.1)
Collecting pycparser
  Using cached pycparser-2.20-py2.py3-none-any.whl (112 kB)
Requirement already satisfied: zope.interface>=4.4.2 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (5.2.0)
Requirement already satisfied: constantly>=15.1 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (15.1.0)
Requirement already satisfied: incremental>=16.10.1 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (17.5.0)
Requirement already satisfied: Automat>=0.3.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.2.0)
Requirement already satisfied: hyperlink>=17.1.1 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.0.1)
Requirement already satisfied: PyHamcrest!=1.10.0,>=1.9.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (2.0.2)
Requirement already satisfied: attrs>=19.2.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.3.0)
Requirement already satisfied: idna!=2.3,>=0.6 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (2.10)
Requirement already satisfied: six>=1.4.1 in ./venv/lib/python3.8/site-packages (from cryptography>=2.7->autobahn>=0.18->daphne<4,>=3.0->channels) (1.15.0)
Requirement already satisfied: attrs>=19.2.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.3.0)
Requirement already satisfied: idna!=2.3,>=0.6 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (2.10)
Collecting pyopenssl>=16.0.0
  Downloading pyOpenSSL-20.0.0-py2.py3-none-any.whl (54 kB)
     |████████████████████████████████| 54 kB 4.4 MB/s
Requirement already satisfied: six>=1.4.1 in ./venv/lib/python3.8/site-packages (from cryptography>=2.7->autobahn>=0.18->daphne<4,>=3.0->channels) (1.15.0)
Collecting service_identity>=18.1.0
  Downloading service_identity-18.1.0-py2.py3-none-any.whl (11 kB)
Requirement already satisfied: attrs>=19.2.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.3.0)
Collecting pyasn1
  Downloading pyasn1-0.4.8-py2.py3-none-any.whl (77 kB)
     |████████████████████████████████| 77 kB 7.8 MB/s
Collecting pyasn1-modules
  Downloading pyasn1_modules-0.2.8-py2.py3-none-any.whl (155 kB)
     |████████████████████████████████| 155 kB 23.8 MB/s
Requirement already satisfied: zope.interface>=4.4.2 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (5.2.0)
Requirement already satisfied: constantly>=15.1 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (15.1.0)
Requirement already satisfied: incremental>=16.10.1 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (17.5.0)
Requirement already satisfied: Automat>=0.3.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.2.0)
Requirement already satisfied: hyperlink>=17.1.1 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.0.1)
Requirement already satisfied: PyHamcrest!=1.10.0,>=1.9.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (2.0.2)
Requirement already satisfied: attrs>=19.2.0 in ./venv/lib/python3.8/site-packages (from twisted[tls]>=18.7->daphne<4,>=3.0->channels) (20.3.0)
Collecting txaio>=20.3.1
  Downloading txaio-20.4.1-py2.py3-none-any.whl (30 kB)
Requirement already satisfied: setuptools in ./venv/lib/python3.8/site-packages (from zope.interface>=4.4.2->twisted[tls]>=18.7->daphne<4,>=3.0->channels) (49.2.1)
Using legacy 'setup.py install' for cffi, since package 'wheel' is not installed.
Building wheels for collected packages: cryptography
  Building wheel for cryptography (PEP 517) ... done
  Created wheel for cryptography: filename=cryptography-3.3.1-cp38-cp38-linux_x86_64.whl size=1181011 sha256=f277173569823c8c2efe4ab58427bf2e0ca474c17a33ccba66032e45daeb2275
  Stored in directory: /home/henrik/.cache/pip/wheels/9b/bd/12/c040f2df6b28138b66b0361cd218180a278b95763fc2466951
Successfully built cryptography
Installing collected packages: pycparser, pyasn1, cffi, pyasn1-modules, cryptography, txaio, service-identity, pyopenssl, autobahn, daphne, channels
    Running setup.py install for cffi ... done
Successfully installed autobahn-20.7.1 cffi-1.14.4 channels-3.0.2 cryptography-3.3.1 daphne-3.0.1 pyasn1-0.4.8 pyasn1-modules-0.2.8 pycparser-2.20 pyopenssl-20.0.0 service-identity-18.1.0 txaio-20.4.1
```

If all this software installs without errors, you should be able to install most software.
